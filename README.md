# README #

Native Java Intelligent Mail Barcode (IMB) Generation

### Motivation ###

The IMB generation tools available on the [USPS Support Center](https://ribbs.usps.gov/index.cfm?page=intellmailmailpieces) are documented in C and require native platform libraries.  Since our primary development platform is Java and we use multiple operating systems for development and testing, a platform-specific libraries are cumbersome to maintain.  We needed a native Java library to generate IMB barcodes.

### Library structure ###

There are only two files in this package, the IMB generator code (no dependencies) and unit tests (requires JUnit, and a couple of apache-commons libraries).  For integration into your project, you will most likely just use the IMB generator.

### License ###

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Contact ###

eric@ijack.net
