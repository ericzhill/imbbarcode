package com.postalpresort.imb;

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.*;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Tests derived from examples in the IMB barcode specification available here:
 * https://ribbs.usps.gov/intelligentmail_mailpieces/documents/tech_guides/SPUSPSG.pdf
 */
@RunWith(JUnit4.class)
public class UnitTests {
    IMBGenerator generator;
    
    @Before
    public void createIMBGenerator() {
        generator = new IMBGenerator();
    }
    
    @Test
    public void testTableBounds() throws Exception {
        assertEquals(6179, generator.code5of13Chars[1270]);
        assertEquals(6275, generator.code5of13Chars[1271]);
        assertEquals(6211, generator.code5of13Chars[1272]);
        assertEquals(5189, generator.code5of13Chars[1273]);
        assertEquals(4681, generator.code5of13Chars[1274]);
        assertEquals(4433, generator.code5of13Chars[1275]);
        assertEquals(4321, generator.code5of13Chars[1276]);
        assertEquals(3142, generator.code5of13Chars[1277]);
    }

    @Test
    public void noZipCode() throws Exception {
        BigInteger binaryEncodedData = generator.binaryEncode("01", "234", "567094", "987654321", "");
        assertEquals("00000000001122103b5c2004b1", Hex.encodeHexString(generator.bigIntTo13Byte(binaryEncodedData)));

        int fcs = generator.calculateFCS(generator.bigIntTo13Byte(binaryEncodedData));
        assertEquals("051", StringUtils.leftPad(Integer.toHexString(fcs), 3, '0'));
    }

    @Test
    public void basicZipCode() throws Exception {
        BigInteger binaryEncodedData = generator.binaryEncode("01", "234", "567094", "987654321", "01234");
        assertEquals("0000000d138a87bab5cf3804b1", Hex.encodeHexString(generator.bigIntTo13Byte(binaryEncodedData)));

        int fcs = generator.calculateFCS(generator.bigIntTo13Byte(binaryEncodedData));
        assertEquals("065", StringUtils.leftPad(Integer.toHexString(fcs), 3, '0'));
    }

    @Test
    public void extendedZipCode() throws Exception {
        BigInteger binaryEncodedData = generator.binaryEncode("01", "234", "567094", "987654321", "012345678");
        assertEquals("000202bdc097711204d21804b1", Hex.encodeHexString(generator.bigIntTo13Byte(binaryEncodedData)));

        int fcs = generator.calculateFCS(generator.bigIntTo13Byte(binaryEncodedData));
        assertEquals("606", StringUtils.leftPad(Integer.toHexString(fcs), 3, '0'));
    }

    @Test
    public void completeZipCode() throws Exception {
        BigInteger binaryEncodedData = generator.binaryEncode("01", "234", "567094", "987654321", "01234567891");
        assertEquals("016907b2a24abc16a2e5c004b1", Hex.encodeHexString(generator.bigIntTo13Byte(binaryEncodedData)));

        int fcs = generator.calculateFCS(generator.bigIntTo13Byte(binaryEncodedData));
        assertEquals("751", StringUtils.leftPad(Integer.toHexString(fcs), 3, '0'));
    }

    @Test
    public void codeWordGeneration() throws Exception {
        BigInteger binaryEncodedData = generator.binaryEncode("01", "234", "567094", "987654321", "01234567891");
        int fcs = generator.calculateFCS(generator.bigIntTo13Byte(binaryEncodedData));

        int[] codeWords = generator.convertToCodeWords(binaryEncodedData);
        assertEquals(14, codeWords[0]);
        assertEquals(787, codeWords[1]);
        assertEquals(607, codeWords[2]);
        assertEquals(1022, codeWords[3]);
        assertEquals(861, codeWords[4]);
        assertEquals(19, codeWords[5]);
        assertEquals(816, codeWords[6]);
        assertEquals(1294, codeWords[7]);
        assertEquals(35, codeWords[8]);
        assertEquals(301, codeWords[9]);

        codeWords = generator.insertAdditionalCodewordData(codeWords, fcs);
        assertEquals(673, codeWords[0]);
        assertEquals(787, codeWords[1]);
        assertEquals(607, codeWords[2]);
        assertEquals(1022, codeWords[3]);
        assertEquals(861, codeWords[4]);
        assertEquals(19, codeWords[5]);
        assertEquals(816, codeWords[6]);
        assertEquals(1294, codeWords[7]);
        assertEquals(35, codeWords[8]);
        assertEquals(602, codeWords[9]);

        codeWords = generator.codeWordsToCharacters(codeWords);
        assertEquals("1234", StringUtils.leftPad(Integer.toHexString(codeWords[0]), 4, '0'));
        assertEquals("085c", StringUtils.leftPad(Integer.toHexString(codeWords[1]), 4, '0'));
        assertEquals("08e4", StringUtils.leftPad(Integer.toHexString(codeWords[2]), 4, '0'));
        assertEquals("0b06", StringUtils.leftPad(Integer.toHexString(codeWords[3]), 4, '0'));
        assertEquals("1922", StringUtils.leftPad(Integer.toHexString(codeWords[4]), 4, '0'));
        assertEquals("1740", StringUtils.leftPad(Integer.toHexString(codeWords[5]), 4, '0'));
        assertEquals("0839", StringUtils.leftPad(Integer.toHexString(codeWords[6]), 4, '0'));
        assertEquals("1200", StringUtils.leftPad(Integer.toHexString(codeWords[7]), 4, '0'));
        assertEquals("0dc0", StringUtils.leftPad(Integer.toHexString(codeWords[8]), 4, '0'));
        assertEquals("04d4", StringUtils.leftPad(Integer.toHexString(codeWords[9]), 4, '0'));

        codeWords = generator.negateCodeWordsWithFCS(codeWords, fcs);
        assertEquals("0dcb", StringUtils.leftPad(Integer.toHexString(codeWords[0]), 4, '0'));
        assertEquals("085c", StringUtils.leftPad(Integer.toHexString(codeWords[1]), 4, '0'));
        assertEquals("08e4", StringUtils.leftPad(Integer.toHexString(codeWords[2]), 4, '0'));
        assertEquals("0b06", StringUtils.leftPad(Integer.toHexString(codeWords[3]), 4, '0'));
        assertEquals("06dd", StringUtils.leftPad(Integer.toHexString(codeWords[4]), 4, '0'));
        assertEquals("1740", StringUtils.leftPad(Integer.toHexString(codeWords[5]), 4, '0'));
        assertEquals("17c6", StringUtils.leftPad(Integer.toHexString(codeWords[6]), 4, '0'));
        assertEquals("1200", StringUtils.leftPad(Integer.toHexString(codeWords[7]), 4, '0'));
        assertEquals("123f", StringUtils.leftPad(Integer.toHexString(codeWords[8]), 4, '0'));
        assertEquals("1b2b", StringUtils.leftPad(Integer.toHexString(codeWords[9]), 4, '0'));

        assertEquals("AADTFFDFTDADTAADAATFDTDDAAADDTDTTDAFADADDDTFFFDDTTTADFAAADFTDAADA", generator.codeWordsToAscenderDescender(codeWords));
    }

    @Test
    public void completeBlankZip() throws Exception {
        assertEquals("ATTFATTDTTADTAATTDTDTATTDAFDDFADFDFTFFFFFTATFAAAATDFFTDAADFTFDTDT",
                generator.encodeIMB("01", "234", "567094", "987654321", ""));
    }

    @Test
    public void manualTest98() throws Exception {
        // 00637814673218466327,28913729838,AFTTADTADTDDDATDFDAFDAFADDTDDDTATADAATAFFADAATFDTTDDDDAFDTFFATFDA,00,Encoding successful
        assertEquals("AFTTADTADTDDDATDFDAFDAFADDTDDDTATADAATAFFADAATFDTTDDDDAFDTFFATFDA",
                generator.encodeIMB("00", "637", "814673", "218466327", "28913729838"));
    }

    @Test
    public void runFullTestSuite() throws Exception {
        int passCount = 0;
        int recordCount = 0;
        FileReader fr = new FileReader("fullTestSuite.csv");
        BufferedReader br = new BufferedReader(fr);
        String line;
        while ((line = br.readLine()) != null) {
            recordCount++;
            String parts[] = line.split(",");
            String barcodeId = StringUtils.substring(parts[1], 0, 2);
            String serviceType = StringUtils.substring(parts[1], 2, 5);
            String mailerId = StringUtils.substring(parts[1], 5, 11);
            String serialNumber = StringUtils.substring(parts[1], 11);
            String zipCode = parts[2];
            String encodedIMB = "";
            String error = "00";

            try {
                encodedIMB = generator.encodeIMB(
                        barcodeId,
                        serviceType,
                        mailerId,
                        serialNumber,
                        zipCode);
            } catch (Exception e) {
                error = "99";
            }

            if (error.equals("00")) {
                assertEquals("Line " + parts[0], parts[4], error);
                assertEquals("Line " + parts[0], parts[3], encodedIMB);
                passCount++;
            } else {
                assertNotEquals("Line " + parts[0], parts[4], error);
                passCount++;
            }
        }

        assertEquals(recordCount, passCount);
    }
}
